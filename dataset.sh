#!/bin/sh
#
# Automatic generation of test data sets for a million
#

#function main
threadnum=$1
min=1

datestart=$(date +%s)
echo "开始生成测试数据集,这可能需要很长一段时间，请稍候..."

while [ $min -le $threadnum ]
do
    ./gentestdata.sh $min $threadnum &
    min=`expr $min + 1`
done

# check script is run complete
loop=true
while($loop)
do
    pidof -x gentestdata.sh > /dev/null
    if [ $? -eq 0 ]; then
        sleep 1
    else
        loop=false
    fi
done

min=1
while [ $min -le $threadnum ]
do
    cat "user.tmp"$min >> user.sql
    cat "product.tmp"$min >> product.sql
    cat "order.tmp"$min >> order.sql
    min=`expr $min + 1`
done
rm -rf *.tmp*
dateend=$(date +%s)
echo "运行耗时：$((dateend-datestart))秒"

exit 0

