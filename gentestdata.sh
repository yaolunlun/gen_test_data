#!/bin/sh
#
# Automatic generation of test data sets for a million
#

total=1000000

#function Rand
#param1: min
#param2: max
#return: random date
Rand()
{  
    local min=$1  
    local max=$(($2-$min+1))  
    local num=$(date +%s%N)  

    echo $(($num%$max+$min))  
}  

#function GenRandString
#param1: length
#return: string
GenRandString()
{
    local length=${1}
    local str=""
    local chars="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz"
    local min=1
    local max=`expr length $chars`

    while [ $min -le $length ]
    do
        local rand=`Rand 0 $max`
        str=$str${chars:rand:1}
        min=`expr $min + 1`
    done

    echo $str
}

#function GenUserData
#param1: threadn
#param2: threadnum
#return: none
GenUserData()
{
    local filename="user.tmp"${1}
    local min=1
    local max=`expr $total / ${2}`

    while [ $min -le $max ]
    do
        name=`GenRandString 5`
        datetime=$(date '+%Y/%m/%d %H:%M:%S')
        email=$name"@sina.com"
        passwd=`echo $passwd|md5sum`
        passwd=${passwd% *}
        echo -e "$name\t$passwd\t$email\t$datetime" >> $filename
        min=`expr $min + 1`
    done
}

#function GenProductData
#param1: threadn
#param2: threadnum
#return: none
GenProductData()
{
    local filename="product.tmp"${1}
    local min=1
    local max=`expr $total / ${2}`

    while [ $min -le $max ]
    do
        title=`GenRandString 10`
        price=`Rand 1 $total`
        uid=`Rand 1 $total`
        datetime=$(date '+%Y/%m/%d %H:%M:%S')
        desc=`GenRandString 50`
        echo -e "$title\t$price\t$uid\t$datetime\t$desc" >> $filename
        min=`expr $min + 1`
    done
}

#function GenOrderData
#param1: threadn
#param2: threadnum
#return: none
GenOrderData()
{
    local filename="order.tmp"${1}
    local min=1
    local max=`expr $total / ${2}`

    while [ $min -le $max ]
    do
        uid=`Rand 1 $total`
        pid=`Rand 1 $total`
        datetime=$(date '+%Y/%m/%d %H:%M:%S')
        echo -e "$uid\t$pid\t$datetime" >> $filename
        min=`expr $min + 1`
    done
}


#function main
threadn=$1
threadnum=$2
#echo `GenRandString 7`
GenUserData $threadn $threadnum
GenOrderData $threadn $threadnum
GenProductData $threadn $threadnum

exit 0

